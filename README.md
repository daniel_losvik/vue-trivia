# vue-trivia

[https://vue-trivia-daniel.herokuapp.com/#/](https://vue-trivia-daniel.herokuapp.com/#/)

This project is made up of five Vue components:
* The Home component where the user chooses a name and starts the game
* The Play component going through all the questions
* The Question component displaying a single question
* The GameOver component showing the result page
* The correctAnswer component showing a single question and the correct answer

## Project setup

### Clone Project
```
git clone git@gitlab.com:daniel_losvik/vue-trivia.git
```

### Install Dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

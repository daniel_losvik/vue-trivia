
export default {

    decodeQuestions(questions) {
        questions.forEach(question => {
          question.category = atob(question.category)
          question.type = atob(question.type)
          question.difficulty = atob(question.difficulty)
          question.question = atob(question.question)
          question.correct_answer = atob(question.correct_answer)
          
          question.incorrect_answers = question.incorrect_answers.map(answer => {
            return atob(answer)
          })
        })
        return questions
    },

    shuffle(array) {
      let counter = array.length;
  
      // While there are elements in the array
      while (counter > 0) {
          // Pick a random index
          let index = Math.floor(Math.random() * counter);
  
          // Decrease counter by 1
          counter--;
  
          // And swap the last element with it
          let temp = array[counter];
          array[counter] = array[index];
          array[index] = temp;
      }
  
      return array;
  },

  sessionStorageSuported() {
    if (typeof(Storage) !== "undefined") {
      return true
    }
    else {
      return false
    }
  }
}
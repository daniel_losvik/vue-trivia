import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import HelperFunctions from './HelperFunctions.js'
import Home from './containers/Home.vue'
import Play from './containers/Play.vue'
import GameOver from './containers/GameOver.vue'

Vue.config.productionTip = false


Vue.use(VueRouter)
Vue.use(BootstrapVue)

Vue.prototype.$HelperFunctions = HelperFunctions

const routes = [
  { path: '/', component: Home },
  { path: '/play', component: Play },
  { path: '/game-over', component: GameOver },
]

const router = new VueRouter({
  routes
})

new Vue({
  render: h => h(App), router
}).$mount('#app')



